<?php
session_start();
$name = $_SESSION["name"];
$gtinh = $_SESSION["gtinh"];
$khoa = $_SESSION["khoa"];
$dchi = $_SESSION["dchi"];
$date = $_SESSION["date"];
$file = $_SESSION["file"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dangki.css">
    <title>Document</title>
</head>
<body>
    <form method="post" action="">
        <div class="name">
            <label class="lab">Họ và tên </label>
            <p class=""><?php echo $name ?></p>
        </div>
        <div class="name">
            <label class="lab">Giới tính</label>
            <p class=""> <?php echo $gtinh ?></p>
        </div>
        <div class="name">
            <label class="lab">Phân Khoa </label>
            <p class=""> <?php echo $khoa ?></p>
        </div>
        <div class="name">
          <label class="lab date" for="date">Ngày sinh </label>
          <p class=""> <?php echo $date ?></p>
        </div>
        <div class="name">
            <label class="lab">Địa chỉ</label>
            <p class=""> <?php echo $dchi ?></p>
        </div>
        <div class="img">
            <div>
            <label class="lab">Hình ảnh</label>
            </div>
            <div>
            <img src="<?php echo $file;?>" alt="" width="150" height="150">
            <!--  -->
            </div>
        </div>
        <button class="dki">Xác nhận</button>
    </form>
</body>
</html>